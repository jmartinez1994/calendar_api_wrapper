require "calendar_api_wrapper/service"
module CalendarApiWrapper
    class TemplateRequest

        attr_reader :service, :body, :client_id, :id, :query

        def call_service
            @service  ||= Service.new
        end

        def create
            call_service.call({ body: body,  type_request:  "Post",   query: query,   url_resource: url_resource })
        end

        def index
            call_service.call({ body: body,  type_request:  "Get",    query: query,   url_resource: url_resource })
        end

        def update 
            call_service.call({ body: body,  type_request: "Patch",   query: query,   url_resource: url_resource_id })	 
        end

        def destroy
            call_service.call({ body: nil,   type_request: "Delete",  query: query,   url_resource: url_resource_id })
        end

        def show
            call_service.call({ body: nil,   type_request: "Get",     query: query,   url_resource: url_resource_id })
        end

        def restore
            call_service.call({ body: body,   type_request: "Patch",  query: query,   url_resource: url_restore })
        end

        def extra( query )
            call_service.call({ body: nil,   type_request: "Get",     query: query,   url_resource: url_resource    })
        end

        def to_json
            return if call_service.nil?
            call_service.response_json
        end

        def to_hash
            return if call_service.nil?
            call_service.response_hash
        end

        def reponse
            return if call_service.nil?
            call_service.reponse
        end

        private 

        def type_resouce
            raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
        end

        def url_resource
            raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
        end

        def url_restore
            raise NotImplementedError, "#{self.class} has not implemented method '#{__method__}'"
        end

        def url_resource_id
            "#{url_resource}/#{id}"
        end

    end
end