require 'net/http'
module CalendarApiWrapper
  class HttpRequest
    attr_accessor :uri, :http, :request, :params, :url_resource, :query

    def initialize(params)
      @url_resource  =  params[:url_resource]
      @query         =  params[:query]
      @uri           =  build_uri
      @http          =  ::Net::HTTP.new(uri.host, uri.port)
      @request       =  "::Net::HTTP::#{params[:type_request]}".constantize.new(uri.path, headers)
      request.body   =  params[:body].to_json
      yield self if block_given?
    end

    def start
      http.use_ssl = true
      http.start{ |protocol| protocol.request(request) }
    end

    private

    def build_uri
      temporal_uri        =   URI("#{configurations.api_calendar_url}/#{url_resource}") 
      temporal_uri.query  =   URI.encode_www_form( params_encode(query) ) if query && query.instance_of?(Array)
      temporal_uri
    end

    def params_encode(queries)
      queries.map do |q|
        [q.first, URI.encode(q.last)]
      end
    end

    def headers
      { 'X-API-KEY' => configurations.api_key, "Content-Type" => "application/json", 'Accept' => 'application/json'}
    end

    def configurations
      CalendarApiWrapper.configuration
    end
  end
end